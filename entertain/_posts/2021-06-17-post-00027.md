---
layout: post
title: "무브 투 헤븐 : 나는 유품정리사 입니다 / 넷플릭스 한국드라마 추천"
toc: true
---

 #1. 안녕하세요? 슛슝이에요.
오늘은 넷플릭스에서 인기리에 방영중인 < 무브 형식 헤븐 : 나는 유품정리사 입니다 > 를 소개해드리려고해요. 보신 많은 분들마다 눈물각이다 감동포텐 터진다며 엄지척해주시는 작품인데요. 저도 많은 드라마를 보는 와중에 챙겨보고있어요. 소개해드릴게요.

#2. 무브 투 헤븐 : 나는 유품정리사 입니다 / 넷플릭스 한국드라마 추천

 | 기본정보
아스퍼거 증후군이 있는 유품정리사 그루와 그의 후견인 상구가 세상을 떠난 이들의 꼴찌 이사를 도우며 그들이 입때껏 전하지 못했던 이야기를 남은 이들에게 대변 전달하는 과정을 담은 넷플릭스 오리지널 시리즈
2021. 5. 14
넷플릭스 오리지널 10부작
연출 김성호
극본 윤지련
출연 이제훈, 탕준상, 홍승희, 정석용, 정영주, 이문식, 임원희, 지진희, 수영

| 메인예고편

 < 무브 기법 헤븐 : 나는 유품정리사 입니다 > 는 고인의 유품을 정리하는 일을 하는 유품정리회사 무브 모드 헤븐의 한정우(지진희), 한그루(탕준상)의 이야기를 담고 있어요. 1화에서는 공장에서 일하다 사고를 당한 청년의 유품을 정리하러 거리 무브 투 헤븐의 이야기를 다루고 있는데요. 한 청년이 어떻게 죽음에 이르게 되었는지 너 청년이 살아생전의 여북 삶을 자작 가꾸었는지, 아울러 남은 사람들이 그를 하여 사랑했는지 등의 이야기들을 다루고 있어요.
1화 후반에는 더군다나 반전으로 스토리가 달라지니 나릿나릿 따라가시며 보면 좋을 것 같아요.

| 캐릭터&캐스팅

• 한그루 역_ 탕준상
아스퍼거 증후군으로 사회적 상호작용이 제대로 되지 않고 반복적인 행동문제를 가진다. 물고기에 관심이 많아 물고기의 이름과 식생을 모조리 외우고 있다.
아버지 한정우(지진희)와 함께 유품정리업체 무브 스타일 헤븐을 운영하고 있다. 아버지의 갑작스러운 사망으로 후견인이 된 아저씨 조상구(이제훈)와 아울러 3개월간 무브 스타일 헤븐을 운영해야한다.

+ 라켓소년단(2021. 5.31~), 사랑의 불시착(조연 금은동) 등으로 활발한 활동을 하고 있는 배우네요. 아스퍼거 증후군을 가진 20살 한그루를 연기하는데 연기가 안정적이고 아스퍼거 증후군에 대해 방금 모르는데도 아 저런 병이구나 싶을 정도로 발달장애연기를 잘하시는 것 같아요. 드라마 포스팅을 하면 할수록 우리나라에 보물같은 배우분들이많다는 생각을 하게 되요. 앞으로의 길도 응원합니다!
☆아스퍼거 증후군이란?
자폐증과 비슷한 발달장애
대인관계에서 상호작용에 어려움이 있고, 관심분야가 한정되는 특징을 보이는 정신과 질환.
(네이버 아스퍼거 증후군 참조)


 • 조상구 역_ 이제훈
아버지가 다른 구성 한정우(지진희)의 죽음으로 한그루(탕준상)의 후견인이 되었다. 후견인이 되면 피후견자의 재산을 마음껏 할 호운 있다기에 한정우가 한그루에게 물려줄 재산이 탐나서 오게 되었는데, 한그루와 3개월동안 정확히 지내야 후견인이 될 핵 있다고 한다. 유품정리업체 무브 식 헤븐 일도 해야하고 아스퍼거 증후군인 한그루(탕준상)와도 십분 지내야한다. 꼴통기질 다분한 그가 진정 후견인이 될 고갱이 있을까?
+ 2021년 5월 인제 방영중인 모범택시에 출연중이신 이제훈 배우님이세요. 근래 밖주인 활발하게 활동하시는 배우님이 아니신가 싶네요. 극 <스토브리그> , 영화 <도굴>, 극 <모범택시>, <무브 형식 헤븐>까지 시거에 매력뿜뿜. 활발한 활동이 케이스 좋네요.

 • 윤나무 역_ 홍승희
한그루의 옆집에서 사는 여자사람친구이자 절친. 그루를 인간적으로 좋아하고 보호하고 싶어하는 인물. 탐탁치않은 그루의 삼촌이자 후견인 후보(?) 조상구(이제훈)의 등장으로 무브 스타일 헤븐에까지 합류하게 된다.

 

#3. 오늘은 넷플릭스에서 많은 인기리에 방영되고 있는 휴먼드라마 < 무브 형식 헤븐 : 나는 유품정리사 입니다 >에 알아봤어요. 대한민국 1세대 유품정리사의 자전적 에세이를 보고 [영화 다시보기](https://imagejoin.com/entertain/post-00002.html) 이빨 작품을 구상했다고 하는데요. 저도 유재석 조세호 님이 나오시는 유퀴즈온더블록에 유품정리사 분이 나오시는 걸 인상깊게 본 비교적 있네요.
많은 인기로 시즌2 제작까지 요청받고 있다는 넷플릭스 오리지널 < 무브 기법 헤븐 : 나는 유품정리사 입니다 > . 10화까지 탄탄한 대본과 연기력으로 가슴저린 감동을 선사한다고 하니, 여러분도 한번 보시면 어떨까요?
https://youtu.be/xhzY5Z-615M

♡읽어주셔서 감사해요♡

end.
